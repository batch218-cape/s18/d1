// console.log("Hello World!");

// function printInfo() {
//     let nickname = prompt ("Enter your nickname: ")
//     console.log("Hi, " + nickname);
// }

// printInfo(); //invoke

                    //parament
function printName (firstName) { 
    console.log("My name is " + firstName);
}

printName("Juana"); //argument
printName("John");
printName("Cena");


//Now we have reusable function / reusable task but could have different output based on what value to process, with the help of...
//[SECTION] Parameters and Arguments

// Parameter
    //"firstName" is called a parameter
    //a "parameter" acts as named variable / container that exist only inside a function
    //it is used to store information that is provided to a function when it is called/ invoke

// Argument
    // "Juana", "John", and "Cena" the information / data provided directly into the function is called argument.
    // Values passed when invoking a function are called an arguments
    // These arguments are then stored as the parameter within the function 

let sampleVariable = "Inday";

printName(sampleVariable);
// Variables can also be passed as an argument 

// -------------------------------------------

function checkDivisibilityBy8 (num) {
    let remainder = num % 8;
    console.log("The remainder of " +num+ " divided by 8 is: " +remainder);
    let isDivisibilityBy8 = remainder === 0; //will store a value true / false
    console.log("Is "+num+" divisible by 8?");
    console.log(isDivisibilityBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// [SECCTION] Function as argument
    //Function parameters can also accept functions arguments
    // Some complete functions uses other functions to perform more complicated results

function argumentFunction () {
    console.log("This function was passed as an argument before the messahe was printed.");
}

function invokeFunction (argumentFunction) {
    argumentFunction();
}

invokeFunction(argumentFunction);
console.log(argumentFunction);


// --------------------------------------------

// [SECTION] Using Multiple Parmeters

function createFullName (firstName, middleName, lastName) {
    console.log(`My full name is ${firstName} ${middleName} ${lastName}`);
}

createFullName("Christopher", "Katigbak", "Malinao");

//Using variables as an argument
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

function getDifferenceOf8Minus4 (numA, numB) {
    console.log(`Difference ${numA - numB}`);
}

getDifferenceOf8Minus4(8, 4);
//getDifferenceOf8Minus4(4, 8); //This will result to logical error

//[SECTION] Return statement

//The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called
function returnFullName (firstName, middleName, lastName) {
    // return `${firstName} ${middleName} ${lastName}`


    let fullName = `${firstName} ${middleName} ${lastName}`
    return fullName;

    // This line of code will not be printed
    console.log("This is printed inside a function");
}



let completeName = returnFullName("Paul", "Smith", "Jordan");
console.log(completeName);

console.log(`I am ${completeName}`);


function printPlayerInfo(userName, level, job) {
    console.log(`Username: ${userName}`);
    console.log(`Level: ${level}`);
    console.log(`Job: ${job}`)
}

let user1 = printPlayerInfo("boxzMapagmahal", "senior", "programmer");
console.log(user1); //returns undefined